Learning `kiwi` and OBS (`osc`) on and for openSUSE
===
**Written by:** _Drew Adams \<druonysus@opensuse.org>_
l
This section where I am keeping notes while teaching myself `kiwi` (more specifically, `kiwi-ng`). I am updating this README file as I learn, and while my main focus with these notes are for myself, I am trying to write them in a way that they **could** be useful to someone else trying to learn kiwi. I will try to be as complete as possible.


# KIWI

In addition to notes/documentation, I will try to add scrips to increase the reproducability of what I have done.

**NOTE:** _Kiwi has been going through a rewite. The new version (sometimes called `kiwi-ng` is written in Python istead of Perl. `kiwi-ng`'s command-line interface is not compatible with the original `kiwi`'s however, the `config.xml` and the process by which they both make images should be roughly the same and generally non-breaking (if there is an incompatibility I am not aware of it, but I do not contribute to `kiwi` or `kiwi-ng`)_

## LINKS
+ [`kiwi-ng` Documentaton](https://opensource.suse.com/kiwi/)
+ https://github.com/SUSE/kiwi-descriptions

## [Setup Build Host](setup_build_host.bash)
[OBS](openbuildservice.org) can build your images using `kiwi` but lets start by just learning `kiwi` locally so we don't conflate OBS and Kiwi's funcgtionality. To do this, lets set up a build host VM. I'm going to trust you can get a VM running openSUSE setup. Once you have that, this section will walk you though setting it up with the



I have put together a script to install kiwi in a way that is not broken. I should likely find the rpm .spec file for it and make sure the `gfxboot` is properly pulled in as a dependecy... but for now just take note that you will need to install `python3-kiwi` **AND** `gfxboot`. The script provided in this repo will  

```bash
./setup_build_host.bash
```

### Installed `kiwi` packages

```
> sudo zypper se -i kiwi 
Loading repository data...
Reading installed packages...

S  | Name           | Summary                                  | Type   
---+----------------+------------------------------------------+--------
i+ | kiwi-man-pages | KIWI - manual pages                      | package
i+ | kiwi-tools     | KIWI - Collection of Boot Helper Tools   | package
i+ | python3-kiwi   | KIWI - Appliance Builder Next Generation | package

```
## Building an image using an existing description