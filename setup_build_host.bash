#!/usr/bin/env bash

zypper ar -f obs://Virtualization:Appliances:Builder kiwi && \
  zypper --gpg-auto-import-keys ref kiwi && \
  zypper -n in python3-kiwi kiwi-tools kiwi-man-pages gfxboot